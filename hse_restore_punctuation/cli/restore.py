import pathlib
from typing import Optional

import click
import pandas as pd
import torch
from torch.utils.data import DataLoader
from transformers import AutoTokenizer, BertForPreTraining, BertTokenizerFast

from hse_restore_punctuation.data import EncodingConfig, PunctuationRestorationDataset
from hse_restore_punctuation.model import BertRestorePunctuation
from hse_restore_punctuation.models.config import CONFIG_NAME, ConfigModel


@click.command()
@click.option("--weights_path", type=pathlib.Path, default=None)
@click.option("--text", prompt=True, type=str)
@click.option(
    "--encoding_path",
    type=pathlib.Path,
    default=pathlib.Path("./data/lentav2/encoding.json"),
)
def restore(
    weights_path: Optional[pathlib.Path], text: str, encoding_path: pathlib.Path
):
    """Restore punctuation for the given text."""
    config = ConfigModel.parse_file(CONFIG_NAME)
    label_encoding = EncodingConfig.parse_file(encoding_path)
    tokenizer: BertTokenizerFast = AutoTokenizer.from_pretrained(config.backbone.name)
    bert = BertForPreTraining.from_pretrained(
        config.backbone.name, output_hidden_states=True
    )

    dataset = PunctuationRestorationDataset(
        pd.DataFrame([{"text": text}]),
        tokenizer=tokenizer,
        label_encoding=label_encoding,
        train=False,
        n_classes=config.data.num_classes,
    )
    dataloader = DataLoader(
        dataset,
        collate_fn=dataset.collate_fn,
        batch_size=1,
        shuffle=config.data.shuffle,
    )

    device = (
        torch.device(config.device)
        if torch.cuda.is_available()
        else torch.device("cpu")
    )

    model = BertRestorePunctuation(bert, config.data.num_classes)
    if weights_path is not None:
        if not weights_path.exists():
            raise click.UsageError(f"Weights for the model not found: {weights_path}")
        with open(weights_path, "rb") as fd:
            model.load_state_dict(torch.load(fd))
    model = model.to(device)
    model.eval()
    for item in dataloader:
        input_ids = item["input_ids"].to(device)
        attention_mask = item["attention_mask"].to(device)
        predicted_ids = torch.argmax(
            model(input_ids, attention_mask=attention_mask), dim=-1
        )
        texts = dataset.batch_restore_punctuation(input_ids, predicted_ids)
        for text in texts:
            click.echo(f"Recovered text: {text}")
