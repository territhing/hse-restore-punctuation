import click


@click.command()
def serve():
    """Serve the model for a human interaction purposes."""
    raise NotImplementedError
