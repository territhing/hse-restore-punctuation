import click

from hse_restore_punctuation.cli.download import download
from hse_restore_punctuation.cli.init import init
from hse_restore_punctuation.cli.restore import restore
from hse_restore_punctuation.cli.serve import serve
from hse_restore_punctuation.cli.train import fit

cli = click.Group(commands=[init, serve, download, fit, restore])  # type: ignore

if __name__ == "__main__":
    cli()
