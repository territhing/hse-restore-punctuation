import pathlib
from typing import Optional

import click
import wandb
from ignite.engine import Events

from hse_restore_punctuation.environment import Environment
from hse_restore_punctuation.evaluate import (
    create_evaluator_from_environment,
    evaluate_model_on_dataset,
)
from hse_restore_punctuation.managers.dataset import Name
from hse_restore_punctuation.train import create_trainer_from_environment
from hse_restore_punctuation.workspace import Workspace


@click.command(no_args_is_help=True)
@click.option(
    "--dataset",
    required=True,
    type=click.Choice([n.value for n in Name]),
    help="Name of the dataset to use for model training.",
)
@click.option(
    "--number-of-snapshots",
    help="Specifies the interval for when to log training metrics.",
    type=int,
    default=10,
)
@click.option(
    "--n-epochs",
    help="Number of epochs to train the model for.",
    type=int,
    default=1,
)
@click.option(
    "--lr-scheduler-steps",
    help="Number of steps to perform with scheduler",
    type=int,
    default=None,
)
def fit(
    dataset: str,
    number_of_snapshots: int,
    n_epochs: int,
    lr_scheduler_steps: Optional[int],
):
    """Train a model on a single GPU."""
    workspace = Workspace(pathlib.Path("."))
    environment = Environment.from_workspace(workspace, Name(dataset))

    scheduler_interval = (
        len(environment.data.train.dataloader) // lr_scheduler_steps
        if lr_scheduler_steps is not None
        else None
    )
    trainer = create_trainer_from_environment(
        environment, scheduler_interval=scheduler_interval
    )
    evaluator = create_evaluator_from_environment(environment)

    wandb.init(project="hse-bert-restore-punctuation", entity="territhing")
    wandb.watch(environment.model)

    snapshot_every_it = len(environment.data.train.dataloader) // number_of_snapshots
    with workspace.experiment_manager.new_experiment() as exp:

        @trainer.on(Events.ITERATION_COMPLETED(every=snapshot_every_it))
        def log_weights_and_metrics() -> None:
            metrics = evaluate_model_on_dataset(
                evaluator,
                environment.data.control.dataloader,
                wandb_prefix="control",
                log_to_wandb=True,
            )
            exp.log(
                {
                    "weights": environment.model.state_dict(),
                    "report": {
                        "metrics": {
                            "accuracy": metrics["accuracy"],
                            "precision": metrics["precision"],
                            "recall": metrics["recall"],
                            "f1_score": metrics["f_score"],
                        },
                        "is_final": False,
                        "classification_report": "",
                    },
                }
            )

        @trainer.on(Events.EPOCH_COMPLETED)
        def finish_training() -> None:
            metrics = evaluate_model_on_dataset(
                evaluator, environment.data.validation.dataloader
            )
            exp.log(
                {
                    "weights": environment.model.state_dict(),
                    "report": {
                        "accuracy": metrics["accuracy"],
                        "precision": metrics["precision"],
                        "recall": metrics["recall"],
                        "f1_score": metrics["f_score"],
                    },
                    "is_final": True,
                    "classification_report": "",
                }
            )

        trainer.run(environment.data.train.dataloader, max_epochs=n_epochs)
