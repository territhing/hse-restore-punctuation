from pathlib import Path

import click

from hse_restore_punctuation.workspace import Workspace


@click.command(no_args_is_help=True)
@click.argument("directory", type=Path, required=True)
def init(directory: Path):
    """
    Initialize a directory for model training. The directory
    must be created beforehand.
    """
    click.echo(f"Initializing a workspace inside {directory}")
    Workspace(directory, needs_validation=False).init()
    click.echo(f"Initialized a workspace inside {directory}")
