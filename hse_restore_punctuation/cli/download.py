from pathlib import Path

import click

from hse_restore_punctuation.managers.dataset import Name
from hse_restore_punctuation.workspace import Workspace


@click.command()
@click.option(
    "--dataset",
    required=True,
    type=click.Choice([n.value for n in Name]),
    help="Name of a dataset to download.",
)
@click.option(
    "--text_column_name",
    type=str,
    default="text",
    help="Name of a column of the dataset which contains text.",
)
@click.option(
    "--n_control_samples",
    type=int,
    default=50,
    help="Number of samples to track metrics on during training.",
)
@click.option(
    "--train_size",
    type=float,
    default=0.7,
    help="A percentage of the dataset that should be used for validation. Must be in range [0, 1].",
)
@click.option(
    "--validation_size",
    type=float,
    default=0.15,
    help="A percentage of the dataset that should be used for validation. Must be in range [0, 1].",
)
def download(
    dataset: str,
    text_column_name: str,
    n_control_samples: int,
    train_size: float,
    validation_size: float,
):
    """Download a dataset into the local storage and preprocess it for training purposes."""
    workspace = Workspace(Path("."))
    workspace.data_manager.get_dataset(Name(dataset)).download(
        text_column_name=text_column_name,
        n_control_samples=n_control_samples,
        train_size=train_size,
        validation_size=validation_size,
    )
