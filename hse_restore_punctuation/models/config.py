import pathlib

from pydantic import BaseModel


class BertModel(BaseModel):
    name: str
    output_hidden_states: bool


class DataModel(BaseModel):
    batch_size: int
    label_encoder_cache_path: pathlib.Path
    num_classes: int
    path: pathlib.Path
    shuffle: bool


class OptimizerModel(BaseModel):
    learning_rate: float


class ConfigModel(BaseModel):
    backbone: BertModel
    data: DataModel
    optimizer: OptimizerModel
    device: str


DEFAULT_CONFIG: ConfigModel = ConfigModel.parse_obj(
    {
        "backbone": {
            "name": "cointegrated/rubert-tiny",
            "output_hidden_states": True,
        },
        "data": {
            "batch_size": 4,
            "label_encoder_cache_path": "label_encoder_cache.json",
            "num_classes": 10,
            "path": "./lenta-ru-news.csv",
            "shuffle": False,
        },
        "optimizer": {
            "learning_rate": 3e-4,
        },
        "device": "cpu",
    }
)
CONFIG_NAME = "hse_restore_punctuation.json"
