from typing import Any, Dict

from pydantic import BaseModel


class Metrics(BaseModel):
    accuracy: float
    precision: float
    recall: float
    f1_score: float


class Report(BaseModel):
    metrics: Metrics
    classification_report: str
    is_final: bool


class ExperimentStatus(BaseModel):
    weights: Dict[Any, Any]
    report: Report
