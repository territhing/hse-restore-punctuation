import pathlib

import click

from hse_restore_punctuation.managers import (
    ConfigManager,
    DataManager,
    ExperimentManager,
)


class Workspace:
    def __init__(self, root: pathlib.Path, needs_validation: bool = True):
        if not root.exists() or not root.is_dir():
            raise click.UsageError(f"{root} does not exist or is not a directory.")
        self._root = root
        self._data_dir = root / "data"
        self._experiment_dir = root / "experiments"
        self._config_manager = ConfigManager(root)
        self._data_manager = DataManager(self._data_dir)
        self._experiment_manager = ExperimentManager(self._experiment_dir)
        if needs_validation:
            self.validate()

    def init(self):
        self.config_manager.create()
        click.echo(f"Creating {self._data_dir}")
        self._data_dir.mkdir()
        click.echo(f"Creating {self._experiment_dir}")
        self._experiment_dir.mkdir()

    @property
    def config_manager(self) -> ConfigManager:
        return self._config_manager

    @property
    def data_manager(self) -> DataManager:
        return self._data_manager

    @property
    def experiment_manager(self) -> ExperimentManager:
        return self._experiment_manager

    def validate(self):
        self.config_manager.validate()
        self.data_manager.validate()
        self.experiment_manager.validate()
