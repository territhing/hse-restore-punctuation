import bz2
import os.path
from pathlib import Path

import requests
from tqdm.auto import tqdm

from hse_restore_punctuation.constants import TQDM_BAR_N_COLS


def download_file_to_local_dir(
    url: str,
    directory: Path,
    *,
    chunk_size: int = 8192,
    print_progress: bool = False,
    exists_ok: bool = True
) -> Path:
    filepath = directory / Path(os.path.basename(url))
    if filepath.exists() and exists_ok:
        return filepath
    filepath.unlink(missing_ok=True)
    with requests.get(url, stream=True, allow_redirects=True) as response:
        response.raise_for_status()
        if print_progress:
            total_size_bytes = int(response.headers.get("content-length", 0))
            progress_bar = tqdm(
                total=total_size_bytes,
                unit="iB",
                unit_scale=True,
                ncols=TQDM_BAR_N_COLS,
            )
        else:
            progress_bar = None
        with open(filepath, "wb") as fd:
            for chunk in response.iter_content(chunk_size=chunk_size):
                fd.write(chunk)
                if progress_bar is not None:
                    progress_bar.update(len(chunk))
        if progress_bar is not None:
            progress_bar.close()
    return filepath


def unzip_bz2(filepath: Path, progress_bar_prefix: str = "") -> Path:
    new_filepath = filepath.with_suffix("")
    description = progress_bar_prefix + new_filepath.name
    with bz2.open(filepath, "rb") as fin, open(new_filepath, "wb") as fout:
        with tqdm(desc=description, unit="B", unit_scale=True) as progress_bar:
            for line in fin:
                progress_bar.update(len(line))
                fout.write(line)
    return new_filepath
