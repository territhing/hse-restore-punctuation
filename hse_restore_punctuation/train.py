from typing import Dict, Optional

import torch
import wandb
from ignite.contrib.handlers import ProgressBar
from ignite.engine import Engine, Events, create_supervised_trainer

from hse_restore_punctuation.constants import TQDM_BAR_N_COLS
from hse_restore_punctuation.environment import Environment


def trainer_prepare_batch(
    batch: Dict[str, torch.Tensor], device: str, non_blocking: bool
) -> (torch.Tensor, torch.Tensor):
    x = batch["input_ids"] * batch["attention_mask"]
    x = x.to(device, non_blocking=non_blocking)
    y = batch["labels"].to(device, non_blocking=non_blocking)
    return x, y


def create_trainer_from_environment(
    environment: Environment, scheduler_interval: Optional[int] = None
) -> Engine:
    trainer: Engine = create_supervised_trainer(
        environment.model,
        environment.optimizer,
        environment.criterion,
        device=environment.device,
        deterministic=True,
        prepare_batch=trainer_prepare_batch,
    )

    training_progress_bar = ProgressBar(ncols=TQDM_BAR_N_COLS, desc="Training model")
    training_progress_bar.attach(trainer)

    @trainer.on(Events.ITERATION_COMPLETED)
    def log_loss(t: Engine) -> None:
        wandb.log({"loss": t.state.output})

    if scheduler_interval is not None:
        scheduler = torch.optim.lr_scheduler.ExponentialLR(
            environment.optimizer, gamma=0.8
        )

        @trainer.on(Events.ITERATION_COMPLETED(every=scheduler_interval))
        def step_schedule() -> None:
            scheduler.step()

    return trainer
