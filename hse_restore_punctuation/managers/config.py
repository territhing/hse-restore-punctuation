import pathlib
from typing import Optional

import click

from hse_restore_punctuation.managers.base import BaseManager
from hse_restore_punctuation.models.config import DEFAULT_CONFIG, ConfigModel


class ConfigManager(BaseManager):
    __config_name__ = "hse_restore_punctuation.json"

    def __init__(self, root: pathlib.Path):
        super().__init__(root)
        self._cfg_path = self._root / self.__config_name__
        self._config: Optional[ConfigModel] = None

    def create(self) -> None:
        click.echo(f"Creating {self._cfg_path}")
        if self._cfg_path.exists():
            raise click.UsageError(f"File {self._cfg_path} already exists.")
        with open(self._cfg_path, "w") as fd:
            fd.write(DEFAULT_CONFIG.json())

    def validate(self) -> None:
        if not self._cfg_path.exists() or not self._cfg_path.is_file():
            raise click.UsageError(f"{self._cfg_path} does not exist or is not a file.")
        _ = ConfigModel.parse_file(self._cfg_path)

    @property
    def config(self) -> ConfigModel:
        if self._config is None:
            self._config = ConfigModel.parse_file(self._cfg_path)
        return self._config
