import pathlib

import click

from hse_restore_punctuation.managers.base import BaseManager
from hse_restore_punctuation.managers.dataset import DatasetManager, Name


class DataManager(BaseManager):
    __available_datasets__ = set(d.value for d in Name)

    def __init__(self, root: pathlib.Path):
        super().__init__(root)

    def validate(self) -> None:
        if not self._root.exists() or not self._root.is_dir():
            raise click.UsageError(
                f"{self._root} does not exist or is not a directory."
            )
        for directory in self._root.iterdir():
            if (
                directory.name in self.__available_datasets__
                and directory.is_dir()
                and not all(directory.iterdir())
            ):
                DatasetManager(directory, Name(directory.name)).validate()

    def get_dataset(self, name: Name) -> DatasetManager:
        dataset_dir = self._root / name.value
        dataset_dir.mkdir(exist_ok=True)
        return DatasetManager(dataset_dir, name)
