import enum
import os.path
import pathlib
import warnings
from collections import Counter
from typing import Dict, List, Optional

import click
import pandas as pd
from tqdm.auto import tqdm

from hse_restore_punctuation.constants import TQDM_BAR_N_COLS
from hse_restore_punctuation.data import (
    EncodingConfig,
    InvalidTokenError,
    strip_punctuation_from_text,
)
from hse_restore_punctuation.managers.base import BaseManager
from hse_restore_punctuation.utils import download_file_to_local_dir, unzip_bz2


class Name(enum.Enum):
    LentaV2 = "lentav2"
    IWSLT = "iwslt"


class Kind(enum.Enum):
    TRAIN = "train"
    VALIDATION = "validation"
    TEST = "test"
    CONTROL = "control"


_NAME_TO_URL_MAP: Dict[Name, str] = {
    Name.LentaV2: "https://github.com/yutkin/Lenta.Ru-News-Dataset/releases/download/v1.1/lenta-ru-news.csv.bz2"
}
_ENCODING_FILE_NAME: str = "encoding.json"


class DatasetManager(BaseManager):
    def __init__(self, root: pathlib.Path, name: Name):
        super().__init__(root)
        self._required_files: Dict[Kind, pathlib.Path] = {
            kind: (root / kind.value).with_suffix(".csv") for kind in Kind
        }
        self._encoding_path: pathlib.Path = self._root / _ENCODING_FILE_NAME
        self._name = name
        self._encoding_config: Optional[EncodingConfig] = None

    def _download_dataset_from_url(self) -> pathlib.Path:
        url = _NAME_TO_URL_MAP[self._name]

        archive_path = self._root / os.path.basename(url)
        raw_data_path = archive_path.with_suffix("")

        if not raw_data_path.exists():
            if not archive_path.exists():
                click.echo(f"Downloading a file from {url}")
                archive_path = download_file_to_local_dir(
                    url, self._root, print_progress=True
                )
            raw_data_path = unzip_bz2(archive_path, f"Extracting {archive_path}: ")
            archive_path.unlink()
            click.echo(f"Dataset '{self._name.value}' downloaded successfully!")
        return raw_data_path

    @staticmethod
    def _count_punctuation_tokens(
        frame: pd.DataFrame, text_column_name: str
    ) -> Counter[str, int]:
        counter: Counter[str, int] = Counter()

        for text in tqdm(
            frame[text_column_name], desc="Building index", ncols=TQDM_BAR_N_COLS
        ):
            if type(text) is not str:
                continue
            try:
                words, punctuation = strip_punctuation_from_text(text)
            except InvalidTokenError:
                continue
            counter.update(punctuation)

        return counter

    def _encode_punctuation_tokens(
        self, distribution: Counter[str, int]
    ) -> Dict[str, int]:
        click.echo("Encoding punctuation marks...")
        distribution: Dict[str, int] = dict(distribution.most_common())
        encoding: Dict[str, int] = {
            token: label
            for label, token in enumerate(distribution.keys(), start=1)
            if token != ""
        }
        encoding[""] = 0
        cfg = EncodingConfig.parse_obj(
            {"distribution": distribution, "encoding": encoding}
        )
        with open(self._encoding_path, "w") as fd:
            fd.write(cfg.json())
        return encoding

    @staticmethod
    def _build_model_dataset(
        df: pd.DataFrame, distribution: Dict[str, int], text_column_name: str
    ) -> pd.DataFrame:
        dataset: List[Dict[str, str]] = []
        for idx, text in tqdm(
            enumerate(df[text_column_name]),
            total=len(df[text_column_name]),
            desc="Creating a dataset",
            ncols=TQDM_BAR_N_COLS,
        ):
            if type(text) is not str:
                continue
            try:
                words, punctuation = strip_punctuation_from_text(text)
            except InvalidTokenError:
                continue
            dataset.append(
                {
                    "text": " ".join(words),
                    "labels": " ".join(
                        list(str(distribution[token]) for token in punctuation)
                    ),
                }
            )
        return pd.DataFrame(dataset)

    def _split_and_save_dataset(
        self,
        df: pd.DataFrame,
        train_size: float,
        validation_size: float,
        n_control_samples: int,
    ):
        click.echo("Splitting and saving datasets...")
        train_df = df.sample(frac=train_size)
        left_df = df.drop(train_df.index)

        percentage_left = 1 - train_size
        validation_size = (percentage_left - validation_size) / percentage_left

        validation_df = left_df.sample(frac=validation_size)
        control_df = validation_df.sample(n_control_samples)
        test_df = left_df.drop(validation_df.index)

        for df, kind in zip([train_df, validation_df, test_df, control_df], Kind):
            df.reset_index(drop=True).to_csv(self._required_files[kind], index=False)

    def download(
        self,
        *,
        text_column_name: str = "text",
        n_control_samples: int = 10,
        train_size: float = 0.7,
        validation_size: float = 0.15,
    ) -> None:
        if train_size < 0 or train_size > 1:
            raise click.UsageError(
                f"Received {train_size=}, expected a value between 0 and 1."
            )
        if validation_size < 0 or validation_size > 1:
            raise click.UsageError(
                f"Received {validation_size=}, expected a value between 0 and 1."
            )
        if n_control_samples < 0:
            raise click.UsageError(
                f"Received {n_control_samples=}, expected a value >= 0."
            )
        present_files = set(self._root.iterdir())
        missing_files = set(self._required_files.values()) - present_files
        if not missing_files:
            return
        data_path = self._download_dataset_from_url()

        click.echo("Loading data into memory...")
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            frame = pd.read_csv(data_path)

        distribution = self._count_punctuation_tokens(frame, text_column_name)
        encoding = self._encode_punctuation_tokens(distribution)
        df = self._build_model_dataset(frame, encoding, text_column_name)
        self._split_and_save_dataset(df, train_size, validation_size, n_control_samples)

    def validate(self) -> None:
        if not self._root.exists() or not self._root.is_dir():
            raise click.UsageError(
                f"{self._root} does not exist or is not a directory."
            )
        for path in self._required_files.values():
            if not path.exists() or not path.is_file():
                raise click.UsageError(f"{path} does not exist or is not a file.")

    @property
    def encoding(self) -> EncodingConfig:
        if self._encoding_config is None:
            self._encoding_config = EncodingConfig.parse_file(self._encoding_path)
        return self._encoding_config

    def get_by_kind(self, kind: Kind) -> pd.DataFrame:
        path = self._root / kind.value
        return pd.read_csv(path.with_suffix(".csv"))
