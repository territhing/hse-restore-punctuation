import datetime as dt
import pathlib
import shutil
import uuid
from types import TracebackType
from typing import Any, Dict, Optional, Type

import click
import torch

from hse_restore_punctuation.managers.base import BaseManager
from hse_restore_punctuation.models.experiment import ExperimentStatus


class Experiment:
    def __init__(self, root: pathlib.Path, experiment_id: str):
        self._root = root
        if not self._root.exists() or not self._root.is_dir():
            raise click.UsageError(
                f"{self._root} does not exist or is not a directory."
            )
        self._weights_dir = root / "weights"
        self._weights_dir.mkdir()
        self._metrics_dir = root / "metrics"
        self._metrics_dir.mkdir()
        self._id = experiment_id

    def log(self, status: Dict[str, Any]) -> None:
        update = ExperimentStatus.parse_obj(status)
        timestamp = dt.datetime.now().strftime("%d-%b-%Y-%H-%M-%S")
        weights_file_path = self._weights_dir / f"weights-{self.id}-{timestamp}.pth"
        metrics_file_path = self._metrics_dir / f"metrics-{self.id}-{timestamp}.json"
        with open(weights_file_path, "wb") as fd:
            torch.save(update.weights, fd)
        with open(metrics_file_path, "w") as fd:
            fd.write(update.report.metrics.json())
        if update.report.is_final:
            with open(self._root / "classification_report.txt", "w") as fd:
                fd.write(update.report.classification_report)

    @property
    def id(self) -> str:
        return self._id

    def __enter__(self) -> "Experiment":
        return self

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc: Optional[BaseException],
        traceback: Optional[TracebackType],
    ) -> Optional[bool]:
        # If no weights were saved, then there is no point in keeping a directory.
        if not any(self._weights_dir.iterdir()):
            shutil.rmtree(self._root)
            return
        name_suffix = "-success" if exc is None else "-fail"
        archive_name = self._root.name + name_suffix
        shutil.make_archive(
            archive_name,
            "zip",
            self._root,
        )
        shutil.rmtree(self._root)


class ExperimentManager(BaseManager):
    def __init__(self, root: pathlib.Path):
        super(ExperimentManager, self).__init__(root)

    def new_experiment(self) -> Experiment:
        experiment_id = str(uuid.uuid4())
        timestamp = dt.datetime.now().strftime("%d-%b-%Y-%H-%M-%S")
        experiment_path = self._root / f"experiment-{experiment_id}-{timestamp}"
        experiment_path.mkdir()
        return Experiment(experiment_path, experiment_id)

    def validate(self) -> None:
        pass
