import abc
import pathlib


class BaseManager(abc.ABC):
    @abc.abstractmethod
    def __init__(self, root: pathlib.Path):
        self._root = root

    @abc.abstractmethod
    def validate(self) -> None:
        raise NotImplementedError
