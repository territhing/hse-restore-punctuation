import dataclasses

import click
import torch
from torch import nn, optim
from torch.utils.data import DataLoader
from transformers import AutoTokenizer, BertForPreTraining, BertTokenizerFast

from hse_restore_punctuation.data import PunctuationRestorationDataset
from hse_restore_punctuation.managers.dataset import Kind, Name
from hse_restore_punctuation.model import BertRestorePunctuation
from hse_restore_punctuation.workspace import Workspace


@dataclasses.dataclass
class DataLoaderPair:
    dataset: PunctuationRestorationDataset
    dataloader: DataLoader

    @classmethod
    def from_workspace(
        cls, workspace: Workspace, name: Name, kind: Kind
    ) -> "DataLoaderPair":
        config = workspace.config_manager.config
        data_mgr = workspace.data_manager.get_dataset(name)
        tokenizer: BertTokenizerFast = AutoTokenizer.from_pretrained(
            config.backbone.name
        )

        dataset = PunctuationRestorationDataset(
            data_mgr.get_by_kind(kind),
            tokenizer=tokenizer,
            label_encoding=data_mgr.encoding,
            train=True,
            n_classes=config.data.num_classes,
        )
        dataloader = DataLoader(
            dataset,
            collate_fn=dataset.collate_fn,
            batch_size=1,
            shuffle=config.data.shuffle,
        )
        return cls(dataset, dataloader)


@dataclasses.dataclass
class Data:
    train: DataLoaderPair
    test: DataLoaderPair
    validation: DataLoaderPair
    control: DataLoaderPair

    @classmethod
    def from_workspace(cls, workspace: Workspace, name: Name) -> "Data":
        click.echo("Loading train data...")
        train = DataLoaderPair.from_workspace(workspace, name, Kind.TRAIN)
        click.echo("Loading test data...")
        test = DataLoaderPair.from_workspace(workspace, name, Kind.TEST)
        click.echo("Loading validation data...")
        validation = DataLoaderPair.from_workspace(workspace, name, Kind.VALIDATION)
        click.echo("Loading control data...")
        control = DataLoaderPair.from_workspace(workspace, name, Kind.CONTROL)
        return Data(train, test, validation, control)


@dataclasses.dataclass
class Environment:
    model: BertRestorePunctuation
    criterion: nn.CrossEntropyLoss
    optimizer: optim.Optimizer
    data: Data
    device: str

    @classmethod
    def from_workspace(cls, workspace: Workspace, dataset_name: Name) -> "Environment":
        config = workspace.config_manager.config
        device = config.device if torch.cuda.is_available() else "cpu"
        click.echo(f"Downloading {config.backbone.name}")
        bert = BertForPreTraining.from_pretrained(
            config.backbone.name, output_hidden_states=True
        )
        click.echo(f"Initializing a model...")
        model = BertRestorePunctuation(bert, config.data.num_classes).to(device)
        criterion = nn.CrossEntropyLoss().to(device)
        optimizer = optim.Adam(model.parameters(), lr=config.optimizer.learning_rate)
        data = Data.from_workspace(workspace, dataset_name)
        return cls(model, criterion, optimizer, data, device)
