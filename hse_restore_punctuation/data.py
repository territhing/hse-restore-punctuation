import re
from collections.abc import Sequence
from typing import Any, Dict, List, Optional, Union

import click
import pandas as pd
import torch
from pydantic import BaseModel
from torch.utils.data import Dataset
from transformers import (
    BatchEncoding,
    BertTokenizer,
    BertTokenizerFast,
    DataCollatorForTokenClassification,
)

UPPERCASE_SUFFIX = "+U"
TokenizerT = Union[BertTokenizer, BertTokenizerFast]


class InvalidTokenError(Exception):
    pass


class EncodingConfig(BaseModel):
    distribution: Dict[str, int]
    encoding: Dict[str, int]


def strip_punctuation_from_text(
    text: str, *, trim_whitespace: bool = False
) -> (List[str], List[str]):
    text_tokens = re.split(r"(\W+)", text)

    # Check whether a text starts with a punctuation token.
    if not text_tokens[0].isalnum():
        raise InvalidTokenError

    word_tokens = text_tokens[::2]
    punctuation_tokens = text_tokens[1::2]

    # Check whether a text ends with a punctuation token.
    # In this case there would be present an empty string as a side
    # effect of the re.split usage.
    if not word_tokens[-1]:
        word_tokens.pop(-1)

    # Check whether a text does not have any punctuation symbol
    # in the end. Generally, that should not happen, but if it is,
    # we must pad punctuation_tokens with exactly one "" token.
    if len(punctuation_tokens) == len(word_tokens) - 1:
        punctuation_tokens.append("")
    assert len(punctuation_tokens) == len(word_tokens)

    for idx, (word, pun) in enumerate(zip(word_tokens, punctuation_tokens)):
        if trim_whitespace:
            if pun.isspace() or not pun[0].isspace() or not pun[-1].isspace():
                punctuation_tokens[idx] = pun.strip()
        if word.islower() or word.isdigit():
            continue
        word_tokens[idx] = word.lower()
        if word.isupper() or word.istitle():
            punctuation_tokens[idx] += UPPERCASE_SUFFIX

    return word_tokens, punctuation_tokens


def restore_punctuation_in_text(
    word_tokens: Sequence[str],
    punctuation_tokens: Sequence[str],
    *,
    tokenizer: Optional[TokenizerT],
) -> str:
    if len(word_tokens) != len(punctuation_tokens):
        raise ValueError(
            "number of word tokens does not match the number of punctuation tokens"
        )
    restored_text = ""
    for word, pun in zip(word_tokens, punctuation_tokens):
        if tokenizer is not None and word in tokenizer.all_special_tokens:
            continue
        if word.startswith("##"):
            word = word[2:]
        word = word.title() if pun.endswith(UPPERCASE_SUFFIX) else word
        pun = pun[:-2] if pun.endswith(UPPERCASE_SUFFIX) else pun
        restored_text += word + pun
    return restored_text


class PunctuationRestorationDataset(Dataset):
    def __init__(
        self,
        data: pd.DataFrame,
        *,
        tokenizer: TokenizerT,
        label_encoding: EncodingConfig,
        text_column: str = "text",
        labels_column: str = "labels",
        truncation: bool = False,
        train: bool = True,
        n_classes: Optional[int] = None,
    ):
        super().__init__()
        self._data = data
        self._tokenizer = tokenizer
        self._text_column = text_column
        self._labels_column = labels_column
        self._label_encoding = label_encoding
        self._inverse_label_encoding: Dict[int, str] = {
            value: key for key, value in label_encoding.encoding.items()
        }
        self._truncation = truncation
        self._train = train
        self._n_classes = n_classes
        # Pad labels with -100 because PyTorch omits it when computing a gradient of nn.CrossEntropyLoss.
        self._collate_fn = DataCollatorForTokenClassification(
            tokenizer=tokenizer, label_pad_token_id=-100, return_tensors="pt"
        )

    def __len__(self) -> int:
        return len(self._data)

    def __getitem__(self, idx: int) -> BatchEncoding:
        row = self._data.iloc[idx]
        if not self._train:
            return self._tokenizer(row[self._text_column], truncation=self._truncation)
        labels: List[int] = []
        for label_id in map(int, row[self._labels_column].split()):
            labels.append(
                label_id if self._n_classes is None or label_id < self._n_classes else 0
            )
        return self.tokenize_and_align_labels(
            {
                "words": row[self._text_column],
                "labels": labels,
            }
        )

    def tokenize_and_align_labels(self, inputs: Dict[str, Any]) -> BatchEncoding:
        tokenized_inputs = self._tokenizer(
            inputs["words"].split(),
            truncation=self._truncation,
            is_split_into_words=True,
        )
        word_labels: List[int] = inputs["labels"]

        prev_word_id = None
        token_labels = []
        for word_id in reversed(tokenized_inputs.word_ids()):
            try:
                if word_id != prev_word_id and word_id is not None:
                    token_labels.append(word_labels[word_id])
                else:
                    token_labels.append(self._label_encoding.encoding[""])
                prev_word_id = word_id
            except Exception:
                from pprint import pformat

                click.echo()
                click.echo(f"Inputs: {inputs}")
                click.echo(f"Tokenized inputs: {tokenized_inputs}")
                click.echo(f"Word IDs {tokenized_inputs.word_ids()}:")
                raise
        token_labels.reverse()
        tokenized_inputs["labels"] = token_labels
        return tokenized_inputs

    def convert_ids_to_tokens(
        self, word_ids: List[int], label_ids: List[int]
    ) -> (List[str], List[str]):
        word_tokens = self._tokenizer.convert_ids_to_tokens(word_ids)
        label_tokens = [
            self._inverse_label_encoding[label_id] for label_id in label_ids
        ]
        return word_tokens, label_tokens

    def batch_restore_punctuation(
        self, input_ids: torch.Tensor, predicted_labels: torch.Tensor
    ) -> List[str]:
        assert input_ids.shape == predicted_labels.shape
        assert len(predicted_labels.shape) == 2
        texts: List[str] = []
        for batch_idx in range(predicted_labels.shape[0]):
            texts.append(
                restore_punctuation_in_text(
                    *self.convert_ids_to_tokens(
                        input_ids[batch_idx].cpu().tolist(),
                        predicted_labels[batch_idx].cpu().tolist(),
                    ),
                    tokenizer=self._tokenizer,
                )
            )
        return texts

    @property
    def collate_fn(self):
        return self._collate_fn
