from typing import Any, Dict

import pandas as pd
import torch
import wandb
from ignite.contrib.handlers import ProgressBar
from ignite.engine import Engine, create_supervised_evaluator
from ignite.metrics import Accuracy, ClassificationReport, Metric, Precision, Recall
from torch.utils.data import DataLoader

from hse_restore_punctuation.constants import TQDM_BAR_N_COLS
from hse_restore_punctuation.environment import Environment
from hse_restore_punctuation.train import trainer_prepare_batch


def get_evaluation_metrics(environment: Environment) -> Dict[str, Metric]:
    return {
        "accuracy": Accuracy(device=environment.device),
        "precision": Precision(average=True, device=environment.device),
        "recall": Recall(average=True, device=environment.device),
        "cr": ClassificationReport(output_dict=True, device=environment.device),
    }


def create_evaluator_from_environment(environment: Environment) -> Engine:
    metrics = get_evaluation_metrics(environment)
    evaluator = create_supervised_evaluator(
        environment.model,
        metrics=metrics,
        device=environment.device,
        prepare_batch=trainer_prepare_batch,
        output_transform=evaluator_output_transform,
    )
    ProgressBar(ncols=TQDM_BAR_N_COLS, desc="Evaluating model").attach(evaluator)
    return evaluator


def evaluate_model_on_dataset(
    evaluator: Engine,
    dataloader: DataLoader,
    wandb_prefix: str = "",
    log_to_wandb: bool = False,
) -> Dict[str, Any]:
    evaluator.run(dataloader)
    metrics = evaluator.state.metrics
    precision = metrics["precision"]
    recall = metrics["recall"]
    metrics["f_score"] = 2 * precision * recall / (precision + recall + 1e-6)
    if log_to_wandb:
        wandb_metrics = {
            f"{wandb_prefix}_{key}": value for key, value in metrics.items()
        }
        wandb_metrics[f"{wandb_prefix}_cr"] = wandb.Table(
            dataframe=pd.DataFrame(list(wandb_metrics[f"{wandb_prefix}_cr"].values()))
        )
        wandb.log(wandb_metrics)
    return metrics


def evaluator_output_transform(
    x: torch.Tensor, y: torch.Tensor, y_pred: torch.Tensor
) -> (torch.Tensor, torch.Tensor):
    return y_pred, y
