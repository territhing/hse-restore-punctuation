import dataclasses

import torch
from torch import nn
from transformers import BertForPreTraining


@dataclasses.dataclass(repr=True)
class _TensorSlice:
    start: int
    end: int


def window_split(
    x: torch.Tensor,
    window_size: int = 512,
    stride: int = 128,
) -> list[_TensorSlice]:
    length = x.size(1)
    splits: list[_TensorSlice] = []

    for slice_start in range(0, length, stride):
        slice_end = min(length, slice_start + window_size)
        splits.append(_TensorSlice(slice_start, slice_end))

        if slice_end == length:
            break

    return splits


class BertRestorePunctuation(nn.Module):
    def __init__(
        self,
        bert: BertForPreTraining,
        num_classes: int,
        hidden_size: int = 512,
        window_size: int = 512,
        stride: int = 128,
    ):
        super().__init__()
        self.bert = bert
        self.embedding_size = self.bert.config.hidden_size
        self.window_size = window_size
        self.stride = stride
        self.hidden_size = hidden_size
        self.num_classes = num_classes
        self.decoder = nn.GRU(
            input_size=self.bert.config.hidden_size,
            hidden_size=self.hidden_size,
            num_layers=4,
            bidirectional=True,
            batch_first=True,
        )
        self.clf = nn.Sequential(
            nn.Linear(2 * self.hidden_size, self.num_classes),
        )

    @torch.no_grad()
    def get_embeddings(self, x: torch.Tensor) -> torch.Tensor:
        return self.bert(x)["hidden_states"][0]

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        batch_size, n_tokens = x.shape
        batch_slices = window_split(x, window_size=self.window_size, stride=self.stride)
        scores = torch.zeros((batch_size, self.num_classes, n_tokens), device=x.device)
        scaling = torch.ones(n_tokens)

        for sls in batch_slices:
            embeddings = self.get_embeddings(x[:, sls.start : sls.end])
            outputs, _ = self.decoder(embeddings)
            result = self.clf(outputs).permute(0, 2, 1)
            scores[:, :, sls.start : sls.end] += result
            scaling[sls.start : sls.end] += 1

        return scores / scaling
