import pathlib
import uuid

import pytest
from click.testing import CliRunner

from hse_restore_punctuation.cli.cli import cli
from hse_restore_punctuation.cli.init import FILES_TO_CREATE
from hse_restore_punctuation.models.config import (
    CONFIG_NAME,
    DEFAULT_CONFIG,
    ConfigModel,
)


@pytest.fixture()
def runner():
    return CliRunner()


def test_init_non_existent_directory(tmp_path: pathlib.Path, runner: CliRunner) -> None:
    dir_path = tmp_path / pathlib.Path(str(uuid.uuid4()))
    assert not dir_path.exists()
    result = runner.invoke(cli, args=["init", str(dir_path)])
    assert result.exit_code != 0


def test_init_bare_directory(tmp_path: pathlib.Path, runner: CliRunner) -> None:
    result = runner.invoke(cli, args=["init", str(tmp_path)])
    assert result.exit_code == 0
    for path in FILES_TO_CREATE:
        current_path = tmp_path / path
        assert current_path.exists()
    cfg_path = tmp_path / CONFIG_NAME
    assert cfg_path.exists()
    cfg = ConfigModel.parse_file(cfg_path)
    assert cfg == DEFAULT_CONFIG
